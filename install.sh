#!/bin/sh
set -eu

GEDIT_PLUGINS_PATH="$HOME/.local/share/gedit/plugins/"
cp ropeplugin.plugin $GEDIT_PLUGINS_PATH
cp -r ropeplugin $GEDIT_PLUGINS_PATH
