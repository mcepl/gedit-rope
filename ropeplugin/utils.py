# This is throwaway dump of code, which might be useful, but I am
# not sure what to do with it now.
import logging
import re

from gi.repository import Gdk

log = logging.getLogger('utils')

# This is old version of the function, which was not used
def __button_release_callback(self, widget, event, data=None):
    if event.get_state() & Gdk.ModifierType.CONTROL_MASK:
        log.debug('goto-definition to be activated')
        doc = self.document
        cursor = doc.get_iter_at_mark(doc.get_insert())
        start = self.__find_word_bound(cursor, False)
        end = self.__find_word_bound(cursor, True)
        log.debug('cursor = %s, start = %s, end = %s',
                  cursor, start, end)
        log.debug('current word = "%s"',
                  doc.get_text(start, end, False))

# This whole set of functions is from
# http://stackoverflow.com/questions/9499923/
def __valid_text(self, start, end):
        if not start or not end:
            log.debug('Invalid start or end')
            return False

        if start.get_line_offset() > end.get_line_offset():
            (start, end) = (end, start)  # swap

        text = self.document.get_text(start, end, False)
        # TODO use somehow dialogs.VALID_PYTHON_IDENTIFIER ?
        return re.match('[_A-Za-z0-9]+$', text) is not None

def __increment(self, index, forward):
    incr = 1 if forward else -1
    newindex = index.copy()
    newindex.set_line_offset(index.get_line_offset() + incr)
    return newindex

def __not_edge_of_line(self, index, forward):
    if not forward:
        # Index is not at the beginning of the line
        return index.get_line_offset() != 0
    else:
        # Index is not at the end of the line
        return not index.ends_line()

# start = self.__find_word_bound(cursor, False)
# end = self.__find_word_bound(cursor, True)
def __find_word_bound(self, index, forward):
    while self.__not_edge_of_line(index, forward):
        newindex = self.__increment(index, forward)
        # newindex contains word?
        if not self.__valid_text(newindex, index):
            break
        # save new index
        index = newindex
    return index
