import logging

import ropemode.environment

from gi.repository import Gedit, Gio, Gtk, GtkSource

log = logging.getLogger(__name__)


class GEditUtils(ropemode.environment.Environment):
    def __init__(self, window):
        super(GEditUtils, self).__init__()
        self.window = window

    @property
    def doc(self):
        return self.window.get_active_document()

    @property
    def view(self):
        return self.window.get_active_view()

    @property
    def enc(self):
        return GtkSource.encoding_get_current().get_charset()

    @property
    def all_documents(self):
        return self.window.get_application().get_documents()

    @property
    def range(self):
        bounds = self.doc.get_selection_bounds()

        if bounds is not None:
            start, end = bounds
            if start.get_line_offset() > end.get_line_offset():
                (start, end) = (end, start)
            return start, end

        return None

    def ask(self, prompt, default=None, starting=None):
        """
        Collect of

        @param prompt: str ....
        @param default: str if users enters nothing, this is the reply
        @param starting: str default reply
        @return str
        """
        pass

    def ask_values(self, prompt, values, default=None, starting=None):
        pass

    def ask_directory(self, prompt, default=None, starting=None):
        '''
        Let user select a dreictory name

        @param prompt str: prompt used
        @param default str: directory to start in
        @param starting IGNORED
        @return str full path of the selected directory (or None)
        '''
        out_dir = None
        dlg = Gtk.FileChooserDialog(
            title=_(prompt),
            action=Gtk.FileChooserAction.SELECT_FOLDER,
            buttons=(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                     Gtk.STOCK_OPEN, Gtk.ResponseType.OK))
        # maybe need to initiate save file dialog if uri == None ?
        if default is not None:
            dlg.set_uri(default)
        dlg.set_default_response(Gtk.ResponseType.OK)
        resp = dlg.run()
        if resp == Gtk.ResponseType.OK:
            out_dir = dlg.get_filename()
        dlg.destroy()
        return out_dir

    def ask_completion(self, prompt, values, starting=None):
        pass

    def message(self, message):
        dlg = Gtk.MessageDialog(self.window,
                                Gtk.DialogFlags.MODAL |
                                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                                Gtk.MessageType.INFO,
                                Gtk.ButtonsType.CLOSE,
                                message)
        dlg.run()
        dlg.destroy()

    def yes_or_no(self, prompt):
        dlg = Gtk.MessageDialog(self.window,
                                Gtk.DialogFlags.MODAL |
                                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                                Gtk.MessageType.QUESTION,
                                Gtk.ButtonsType.YES_NO,
                                prompt)
        dlg.run()
        dlg.destroy()

    def y_or_n(self, prompt):
        dlg = Gtk.MessageDialog(self.window,
                                Gtk.DialogFlags.MODAL |
                                Gtk.DialogFlags.DESTROY_WITH_PARENT,
                                Gtk.MessageType.QUESTION,
                                Gtk.ButtonsType.NONE,
                                prompt)
        dlg.add_buttons(['y', 'n'])
        dlg.run()
        dlg.destroy()

    def get(self, name, default=None):
        # def get(self, name, default=None):
        #        vimname = 'g:ropevim_%s' % name
        #        if str(vim.eval('exists("%s")' % vimname)) == '0':
        #            return default
        #        result = vim.eval(vimname)
        #        if isinstance(result, str) and result.isdigit():
        #            return int(result)
        #        return result
        #
        #    def get(self, name, default=None):
        #        lispname = 'ropemacs-' + name.replace('_', '-')
        #        if lisp.boundp(lisp[lispname]):
        #            return lisp[lispname].value()
        #        return default
        pass

    def get_offset(self):
        return self.doc.get_insert.get_line_offset()

    def get_text(self):
        return self.doc.get_text(self.doc.get_start_iter(),
                                 self.doc.get_end_iter(),
                                 False).decode(self.enc)

    def get_region(self):
        """
        return text inside of selection or None if there is None.
        """
        bounds = self.range()

        if bounds is not None:
            return self.doc.get_text(bounds[0], bounds[1],
                                     False).decode(self.enc)

    def filename(self):
        """
        Return full path of the current document as a string.
        """
        return self.doc.get_location().get_path()

    def is_modified(self):
        return self.doc.get_modified()

    def _file_equals(self, first, second):
        def get_file_id(f):
            return f.query_info(
                'id::file',
                Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS,
                None).get_attribute_as_string('id::file')

        first_id = get_file_id(first)
        second_id = get_file_id(second)
        return first_id and second_id and first_id == second_id

    def goto_line(self, lineno):
        view = self.window.get_active_view()
        log.debug('buf = %s, lineno = %d',
                  view.get_buffer().get_location().get_path(),
                  lineno)
        it = Gtk.TextIter()
        view.get_buffer().get_iter_at_line(it, lineno)
        log.debug('iterator = %s, %d', it, it.get_line())
        it.set_line(lineno)
        log.debug('iterator = %s, %d', it, it.get_line())
        log.debug('scrolling to iter!')
        # self.window.get_active_view().scroll_to_iter(it, 0, True, 0.5, 0.5)
        view.get_buffer().select_range(it, it)
        begin, end = view.get_buffer().get_bounds()
        log.debug("%s %s" % (begin, end))
        log.debug("%u %u" % (begin.get_line(), end.get_line()))
        # view.get_buffer().goto_line(lineno)
        # view.scroll_to_cursor()

    def insert_line(self, line, lineno):
        old_insert = self.doc.get_insert()
        cursor = self.doc.get_iter_at_mark(old_insert)

        cursor.set_line(lineno)
        self.doc.insert(cursor, '\n' + line)

        # return to the old position
        self.doc.place_cursor(self.doc.get_iter_at_mark(old_insert))

    def insert(self, text):
        self.view.get_buffer().insert_at_cursor(text)

    def delete(self, start, end):
        bounds = self.range()

        if bounds is not None:
            self.doc.delete(bounds[0], bounds[1])

    def filenames(self):
        result = []
        for buffer in self.all_documents:
            if not buffer.is_untitled():
                result.append(buffer.get_location().get_path())
        return result

    def save_files(self, filenames):
        Gedit.commands_save_all_documents(self.window)

    def reload_files(self, filenames, moves={}):
        pass

    def find_file(self, filename, readonly=False, other=False, force=False):
        # TODO: get some configuration:
        #       * other=self.env.get('goto_def_newwin'))
        other_file = Gio.File.new_for_commandline_arg(filename)
        doc = self.filename()
        doc_gfile = Gio.File.new_for_commandline_arg(doc)

        if not self._file_equals(doc_gfile, other_file) or force:
            for view in self.window.get_views():
                buf = view.get_buffer()
                if self._file_equals(buf.get_location(), other_file):
                    tab = view.get_ancestor(Gedit.Tab)
                    self.window.set_active_tab(tab)
                    return tab

            tab = self.window.create_tab_from_location(
                location=other_file, encoding=GtkSource.encoding_get_current(),
                line_pos=0, column_pos=0, create=False, jump_to=True)
            self.window.set_active_tab(tab)
            return tab

        if readonly:
            # FIXME vim.command('set nomodifiable')
            pass

    def create_progress(self, name):
        pass

    def current_word(self):
        # return vim.eval('expand("<cword>")')
        pass

    def push_mark(self):
        # vim.command('mark `')
        pass

    def pop_mark(self):
        pass

    def prefix_value(self, prefix):
        return prefix

    def show_occurrences(self, locations):
        pass

    def show_doc(self, docs, altview=False):
        pass

    def preview_changes(self, diffs):
        pass

    def local_command(self, name, callback, key=None, prefix=False):
        pass

    def global_command(self, name, callback, key=None, prefix=False):
        pass

    def add_hook(self, name, callback, hook):
        pass

    def _completion_text(self, proposal):
        return proposal.name

    def _completion_data(self, proposal):
        return self._completion_text(proposal)
